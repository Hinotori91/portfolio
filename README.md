# Portfolio
> [Switch to German](README_DE.md)

## About me
I am currently training as an application developer at CODERS.BAY Vienna. I have knowledge of web technologies, including HTML, CSS, JavaScript, and PHP. I also have experience with frameworks such as Laravel and Vue.js. I am also familiar with Java, OOP, Hibernate and Spring Boot. I have also worked in the field of mobile applications in Android Studio with Kotlin and Jetpack Compose.

In this portfolio I would like to present some of my work and hope that you will like these. Further projects are on my Gitlab as well as on my GitHub account and can be viewed.

## iDreck - The triangle calculator
- https://github.com/Hinotori91/iDreck


With this triangle calculator it is possible to calculate the Side-Side-Angle-Set (SSW), the Side-Side-Side-Set (SSS) and the Side-Angle-Angle-Set (SWW).

With the specification of the choosen sets, the other sides with their angles are calculated. The radian measure of all angles, as well as the perimeter, the area, the circumcircle radius, the incircle radius and all side heights are calculated too.

The triangle is also drawn and the corners are labeled.

The calculator is also hosted via GitHub Pages an can be tested at this link
- https://hinotori91.github.io/iDreck/

> Currently, the operation is only possible in German.

![iDreck](img/190982456-6f60ad63-3543-4c90-a0af-8c2f7cfee449.png)

## LaMa - Learn and Management App
- https://gitlab.com/Hinotori91/lama-learn-and-management-app

With this Android app it is possible to create, edit and delete subjects, topics, questions and answers. In addition, there is a possibility to learn the stored questions with their answers. In this regard, an algorithm runs in the background that outputs the worst-answered question based on a weighting and presents it to the user for learning.

It is possible to learn all questions about a subject, or all questions about a topic.

There is also a calendar where dates and events can be entered. So you could e.g. upcoming exams, submissions or scheduled ToDos entered and read out again.

There is also a ToDo list so that you don't forget anything.

<img src="img/Startseite.png" width="350"> 

|                                          |                                               |
| :--------------------------------------: | :-------------------------------------------: |
| <img src="img/Faecher.png" width="350">  | <img src="img/Themengebiete.png" width="350"> |
|  <img src="img/Frage.png" width="350">   |    <img src="img/Antwort.png" width="350">    |
| <img src="img/Kalender.png" width="350"> |     <img src="img/ToDo.png" width="350">      |

### Frontend

The app was created with Android Studio in the Kotlin language with the Jetpack Compose framework. Elements of Material3 were also used.

### Backend

The backend was written in Java with the SpringBoot framework. The project for the backend is linked below.
- https://github.com/Hinotori91/Exam-CRUD-Backend

### Database
The database that runs in the background is an Apache XAMPP MySQL database that is automatically created when the Java project is first run.