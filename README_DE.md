# Portfolio
>> [Switch to English](README.md)

## Über Mich
Zurzeit bin ich in einer Ausbildung zur Applikationsentwicklerin bei CODERS.BAY Vienna Ich habe Kenntnisse in verschiedenen Webtechnologien, darunter HTML, CSS, JavaScript, und PHP. Ich habe auch Erfahrung mit Frameworks wie Laravel und Vue.js. Ebenso bin ich vertraut mit Java, OOP, Hibernate und Spring Boot. Ebenfals habe ich im zuge der Ausbildung auch im Bereich Mobile Applications in Android Studio mit Kotlin und Jetpack Compose gearbeitet.

In diesem Portfolio möchte ich gerne einige meiner Arbeiten vorstellen und hoffe das diese Projekte auch gefallen finden. Weitere Projekte sind sowohl auf meinem Gitlab als auch auf meinem GitHub Account und können gerne begutachtet werden.

## iDreck - Der Dreicksrechner
- https://github.com/Hinotori91/iDreck

Mit diesem Dreicksrechner ist es möglich den Seiten-Seiten-Winkel-Satz (SSW), den Seiten-Seiten-Seiten-Satz (SSS) und den Seiten-Winkel-Winkel-Satz (SWW) zu berechnen.

Mit der Angabe von den jeweiligen Sätzen, werden die anderen Seiten mit deren Winkeln berechnet. Ebenso wird das Bogenmaß aller Winkel, sowie der Umfang, der Flächeninhalt, der Umkreisradius, der Inkreisradius und alle Seitenhöhen berechnet.

Zusätzlich wird das Dreieck auch gezeichnet und die Ecken beschriftet.

Dieser Rechner wurde auch über GitHub Pages gehostet und kann unter diesem Link getestet werden.
- https://hinotori91.github.io/iDreck/

> Derzeit ist die Bedienung allerdings nur in deutsch möglich.

![iDreck](img/190982456-6f60ad63-3543-4c90-a0af-8c2f7cfee449.png)


## LaMa - Learn and Management App

- https://gitlab.com/Hinotori91/lama-learn-and-management-app

<img src="img/Lama__.png" width="350">

Mit dieser Android App ist es möglich Fächer, Themengebiete, Fragen und Antworten zu erstellen, zu bearbeiten und zu löschen. Zusätzlich gibt es eine Möglichkeit die hinterlegten Fragen mit dessen Antworten zu lernen. Diesbezüglich läuft im Hintergrund ein Algorithmus der anhand von einer Gewichtung die am schlecht beantworteste Frage ausgibt und dem User zum lernen vorlegt.

Es ist Möglich alle Fragen zu einem Fach, oder alle Fragen zu einem Themengebiet zu lernen.

Es gibt auch einen Kalender wo Termine und Events eingetragen werden können. So könnte man zb anstehende Prüfungen, Abgaben oder terminierte ToDos eingetragen und wieder ausgelesen werden.

Ebenso gibt es eine ToDo Liste um nichts zu vergessen.

<img src="img/Startseite.png" width="350"> 

|                                          |                                               |
| :--------------------------------------: | :-------------------------------------------: |
| <img src="img/Faecher.png" width="350">  | <img src="img/Themengebiete.png" width="350"> |
|  <img src="img/Frage.png" width="350">   |    <img src="img/Antwort.png" width="350">    |
| <img src="img/Kalender.png" width="350"> |     <img src="img/ToDo.png" width="350">      |

### Frontend
Erstellt wurde die App mit Android Studio in der Sprache Kotlin mit dem Framework Jetpack Compose. Es wurden auch Elemente von Material3 verwendet.

### Backend
Das Backend wurde mit Java mit dem Framework SpringBoot geschrieben. Das Projekt fürs Backend verlinke ich darunter.
- https://github.com/Hinotori91/Exam-CRUD-Backend

### Datenbank
Die Datenbank die im Hintergrund läuft ist eine Apache XAMPP MySQL Datenbank, die beim ersten ausführen des Java Projekts automatisch erstellt wird.
